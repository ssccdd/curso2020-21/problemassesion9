/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion9.grupo6;

import static es.uja.ssccdd.curso2021.problemassesion9.grupo6.Utils.TIEMPO_ESPERA_CENTRO;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo6.Utils.PACIENTE_EDAD_MINIMA;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo6.Utils.PACIENTE_EDAD_MAXIMA;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class CentroMedico implements Runnable {

    private final int iD;
    private final PriorityBlockingQueue<Paciente> pacientes;
    private final AtomicInteger contIdentificadores;
    private final AtomicIntegerArray registroTrabajo;

    public CentroMedico(int iD, PriorityBlockingQueue<Paciente> pacientes, AtomicInteger contIdentificadores, AtomicIntegerArray registroTrabajo) {
        this.iD = iD;
        this.pacientes = pacientes;
        this.contIdentificadores = contIdentificadores;
        this.registroTrabajo = registroTrabajo;
        ThreadLocalRandom.current();
    }

    private int getSiguienteID() {
        return contIdentificadores.getAndIncrement();
    }

    @Override
    public void run() {

        boolean interrumpido = false;

        System.out.println("Centro " + iD + " ha empezado.");

        while (!interrumpido) {

            try {

                int edad = ThreadLocalRandom.current().nextInt(PACIENTE_EDAD_MAXIMA - PACIENTE_EDAD_MINIMA) + PACIENTE_EDAD_MINIMA;
                pacientes.add(new Paciente(getSiguienteID(), edad));
                registroTrabajo.getAndIncrement(iD);

                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_CENTRO);
            } catch (InterruptedException ex) {
                interrumpido = true;
            }

        }

        System.out.println("El centro médico " + iD + " ha terminado el trabajo.\tTotal pacientes derivados: " + registroTrabajo.get(iD));
    }

}
