/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion9.grupo5;

import static es.uja.ssccdd.curso2021.problemassesion9.grupo5.Utils.CalidadImpresion;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo5.Utils.TIEMPO_ESPERA_CLIENTE;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Cliente implements Runnable {

    private final int iD;
    private final BlockingDeque<Modelo> listaDePeticiones;
    private final CalidadImpresion calidad;
    private final AtomicInteger contIdentificadores;
    private final AtomicIntegerArray registroTrabajo;

    public Cliente(int iD, BlockingDeque<Modelo> listaDePeticiones, CalidadImpresion calidad, AtomicInteger contIdentificadores, AtomicIntegerArray registroTrabajo) {
        this.iD = iD;
        this.listaDePeticiones = listaDePeticiones;
        this.calidad = calidad;
        this.contIdentificadores = contIdentificadores;
        this.registroTrabajo = registroTrabajo;
    }

    private int getSiguienteID() {
        return contIdentificadores.getAndIncrement();
    }

    @Override
    public void run() {

        System.out.println("Cliente " + iD + " ha empezado.");

        try {

            while (true) {

                listaDePeticiones.put(new Modelo(getSiguienteID(), calidad));
                registroTrabajo.getAndIncrement(iD);
                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_CLIENTE);

            }

        } catch (InterruptedException ex) {
            //Si nos interrumpen no tenemos que modificar nada.
        }

        System.out.println("Cliente " + iD + " ha terminado por interrupción.\tTotal modelos pedidos: " + registroTrabajo.get(iD));

    }

}
