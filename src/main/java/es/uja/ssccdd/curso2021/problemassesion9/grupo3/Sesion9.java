/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion9.grupo3;

import static es.uja.ssccdd.curso2021.problemassesion9.grupo2.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo3.Constantes.AFORO;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo3.Constantes.NUM_CLIENTES;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo3.Constantes.NUM_ESTANTERIAS;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo3.Constantes.NUM_PRODUCTOS;
import es.uja.ssccdd.curso2021.problemassesion9.grupo3.Constantes.Producto;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo3.Constantes.TIEMPO_REPOSICION;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo3.Constantes.aleatorio;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 *
 * @author pedroj
 */
public class Sesion9 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        ScheduledExecutorService ejecucion;
        AtomicIntegerArray[] supermercado;
        Reponedor reponedor;
        Cliente cliente;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicializamos las variables del sistema
        ejecucion = Executors.newScheduledThreadPool(AFORO);
        supermercado = new AtomicIntegerArray[NUM_ESTANTERIAS];
        for(int i = 0; i < NUM_ESTANTERIAS; i++) {
            supermercado[i] = new AtomicIntegerArray(NUM_PRODUCTOS);
            for(Producto producto : Constantes.Producto.values())
                supermercado[i].set(producto.ordinal(), producto.getUnidades());
        }
        
        // Se crean los reponedores y se planifica su ejecución
        for(int i = 0; i < NUM_ESTANTERIAS; i++) {
            reponedor = new Reponedor("Reponedor(" + i + ")", i, supermercado);
            ejecucion.scheduleAtFixedRate(reponedor, TIEMPO_REPOSICION, TIEMPO_REPOSICION, TimeUnit.SECONDS);
        }
        
        // Se crean y ejecutan los clientes
        for(int i = 0; i < NUM_CLIENTES; i++) {
            int estanteria = aleatorio.nextInt(NUM_ESTANTERIAS);
            cliente = new Cliente("Reponedor(" + i + ")", estanteria, supermercado);
            ejecucion.execute(cliente);
        }
        
        // Ejecutamos por un tiempo
        System.out.println("(HILO_PRINCIPAL) Espera a por un tiempo a que los clientes realicen compras");
        TimeUnit.SECONDS.sleep(TIEMPO_ESPERA);
        
        // Se solicita la cancelación de las tareas que no han finalizado
        System.out.println("(HILO_PRINCIPAL) Solicita la finalización");
        ejecucion.shutdownNow();
        ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
}
