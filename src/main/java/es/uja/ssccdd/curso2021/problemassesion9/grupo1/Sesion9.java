/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion9.grupo1;

import static es.uja.ssccdd.curso2021.problemassesion9.grupo1.Constantes.MAX_AFORO;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo1.Constantes.MAX_ENTRADAS;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo1.Constantes.MIN_AFORO;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo1.Constantes.MIN_ENTRADAS;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo1.Constantes.NUM_PERSONAS;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo1.Constantes.NUM_SALAS;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo1.Constantes.PUNTOS_VENTA;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo1.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo1.Constantes.aleatorio;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 *
 * @author pedroj
 */
public class Sesion9 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        ExecutorService ejecucion;
        AtomicIntegerArray aforoSalas;
        Persona persona;
        int aforo;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicializamos las variables del sistema
        ejecucion = Executors.newFixedThreadPool(PUNTOS_VENTA);
        aforoSalas = new AtomicIntegerArray(NUM_SALAS);
        for(int i = 0; i < NUM_SALAS; i++) {
            aforo = aleatorio.nextInt(MAX_AFORO - MIN_AFORO + 1) + MIN_AFORO;
            aforoSalas.set(i, aforo);
        }
        
        // Se crean las personas y se ejecutan
        for(int i = 1; i <= NUM_PERSONAS; i++) {
            int sala = aleatorio.nextInt(NUM_SALAS);
            int entradas = aleatorio.nextInt(MAX_ENTRADAS - MIN_ENTRADAS + 1) + MIN_ENTRADAS;
            persona = new Persona("Persona(" + i + ")", entradas, sala, aforoSalas);
            ejecucion.execute(persona);
        }
        
        // Ejecutamos por un tiempo
        System.out.println("(HILO_PRINCIPAL) Espera a por un tiempo a que las personas compren entradas");
        TimeUnit.SECONDS.sleep(TIEMPO_ESPERA);
        
        // Se solicita la cancelación de las tareas que no han finalizado
        System.out.println("(HILO_PRINCIPAL) Solicita la finalización");
        ejecucion.shutdownNow();
        ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
}
