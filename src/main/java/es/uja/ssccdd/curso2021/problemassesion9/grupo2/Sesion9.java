/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion9.grupo2;

import static es.uja.ssccdd.curso2021.problemassesion9.grupo2.Constantes.EMPLEADOS;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo2.Constantes.LIBRE;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo2.Constantes.MAX_EQUIPAJE;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo2.Constantes.MIN_EQUIPAJE;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo2.Constantes.NUM_CONSIGNAS;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo2.Constantes.NUM_PERSONAS;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo2.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo2.Constantes.aleatorio;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 *
 * @author pedroj
 */
public class Sesion9 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        ExecutorService ejecucion;
        AtomicIntegerArray consignas;
        int[] capacidadConsigna;
        Persona persona;
        int capacidad;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicializamos las variables del sistema
        ejecucion = Executors.newFixedThreadPool(EMPLEADOS);
        capacidadConsigna = new int[NUM_CONSIGNAS];
        consignas = new AtomicIntegerArray(NUM_CONSIGNAS);
        for(int i = 0; i < NUM_CONSIGNAS; i++) {
            capacidad = aleatorio.nextInt(MAX_EQUIPAJE - MIN_EQUIPAJE + 1) + MIN_EQUIPAJE;
            capacidadConsigna[i] = capacidad;
            consignas.set(i, LIBRE);
        }
        
        // Se crean las personas y se ejecutan
        for(int i = 1; i <= NUM_PERSONAS; i++) {
            int equipaje = aleatorio.nextInt(MAX_EQUIPAJE - MIN_EQUIPAJE + 1) + MIN_EQUIPAJE;
            persona = new Persona("Persona(" + i + ")", equipaje, consignas, capacidadConsigna);
            ejecucion.execute(persona);
        }
        
        // Ejecutamos por un tiempo
        System.out.println("(HILO_PRINCIPAL) Espera a por un tiempo a que las personas interactuen con las consignas");
        TimeUnit.SECONDS.sleep(TIEMPO_ESPERA);
        
        // Se solicita la cancelación de las tareas que no han finalizado
        System.out.println("(HILO_PRINCIPAL) Solicita la finalización");
        ejecucion.shutdownNow();
        ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
}
