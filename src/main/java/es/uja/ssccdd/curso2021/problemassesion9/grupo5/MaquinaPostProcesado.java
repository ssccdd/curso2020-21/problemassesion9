/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion9.grupo5;

import static es.uja.ssccdd.curso2021.problemassesion9.grupo5.Utils.TIEMPO_ESPERA_MAQUINA;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class MaquinaPostProcesado implements Runnable {

    private final int iD;
    private final DelayQueue<Modelo> modelosImpresos;
    private final BlockingDeque<Modelo> modelosPostprocesados;
    private final AtomicIntegerArray registroTrabajo;

    public MaquinaPostProcesado(int iD, DelayQueue<Modelo> modelosImpresos, BlockingDeque<Modelo> modelosPostprocesados, AtomicIntegerArray registroTrabajo) {
        this.iD = iD;
        this.modelosImpresos = modelosImpresos;
        this.modelosPostprocesados = modelosPostprocesados;
        this.registroTrabajo = registroTrabajo;
    }

    @Override
    public void run() {


        boolean interrumpido = false;

        System.out.println("Maquina de postprocesado " + iD + " iniciada");

        while (!interrumpido) {

            try {

                Modelo modeloAProcesar = modelosImpresos.take();

                modelosPostprocesados.put(modeloAProcesar);

                registroTrabajo.getAndIncrement(iD);

                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_MAQUINA);
            } catch (InterruptedException ex) {
                interrumpido = true;
            }
        }

        System.out.println("La máquina " + iD + " ha terminado el trabajo.\tTotal modelos procesados: " + registroTrabajo.get(iD));

    }

}
