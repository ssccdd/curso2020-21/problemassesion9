package es.uja.ssccdd.curso2021.problemassesion9.grupo1;

import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pedroj
 */
public interface Constantes {
    public static final Random aleatorio = new Random();
    
    // Constantes
    public static final int NUM_PERSONAS = 100;
    public static final int MIN_ENTRADAS = 1;
    public static final int MAX_ENTRADAS = 5;
    public static final int MIN_AFORO = 25;
    public static final int MAX_AFORO = 50;
    public static final int NUM_SALAS = 5;
    public static final int PUNTOS_VENTA = 3;
    public static final int ENTRADAS_AGOTADAS = 0;
    public static final int TIEMPO_VENTA = 1;
    public static final int TIEMPO_ESPERA = 30; // segundos
    
}
