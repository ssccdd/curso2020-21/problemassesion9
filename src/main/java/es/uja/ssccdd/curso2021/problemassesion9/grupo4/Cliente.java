/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion9.grupo4;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Cliente implements Runnable {

    private final int iD;
    private final BlockingDeque<MenuReparto> listaDeMenus;
    private final AtomicInteger contIdentificadores;

    public Cliente(int iD, BlockingDeque<MenuReparto> listaDeMenus, AtomicInteger contIdentificadores_) {
        this.iD = iD;
        this.listaDeMenus = listaDeMenus;
        contIdentificadores = contIdentificadores_;
    }

    private int getSiguienteID() {
        return contIdentificadores.getAndIncrement();
    }

    @Override
    public void run() {

        System.out.println("Cliente " + iD + " ha empezado.");

        int idMenu = getSiguienteID();

        try {
            listaDeMenus.put(new MenuReparto(idMenu));
            System.out.println("Cliente " + iD + " ha terminado correctamente.");
        } catch (InterruptedException ex) {
            //Si nos interrumpen no hacemos nada, porque no hay bucle que parar.
            System.out.println("Cliente " + iD + " ha terminado por interrupción.");
        }

    }

}
