/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion9.grupo3;

import es.uja.ssccdd.curso2021.problemassesion9.grupo3.Constantes.Producto;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicIntegerArray;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo3.Constantes.TIEMPO_PRODUCTO;

/**
 *
 * @author pedroj
 */
public class Reponedor implements Runnable {
    private final String iD;
    private final int estanteria;
    private final AtomicIntegerArray[] supermercado;

    public Reponedor(String iD, int estanteria, AtomicIntegerArray[] supermercado) {
        this.iD = iD;
        this.estanteria = estanteria;
        this.supermercado = supermercado;
    }
    
    @Override
    public void run() {
        System.out.println("TAREA-" + iD + " Empieza su ejecución");
        
        try {
            
            reponerEstanteria();    
            
            System.out.println("Ha finalizado la ejecución del TAREA-" + iD);
        } catch (InterruptedException ex) {
            System.out.println("Se ha interrumpido la ejecución del TAREA-" + iD + ex);
        }
    }
    
    private void reponerEstanteria() throws InterruptedException {
        // Comprueba la solicitud de interrupción de la tarea antes de comenzar
        if ( Thread.interrupted() )
           throw new InterruptedException();
   
        // Se reponen los productos de una estantería
        for( Producto producto : Producto.values() ) {
            supermercado[estanteria].getAndSet(producto.ordinal(), producto.getUnidades());
            TimeUnit.SECONDS.sleep(TIEMPO_PRODUCTO);
        }
    }
}
