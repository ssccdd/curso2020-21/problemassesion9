/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion9.grupo2;

import java.util.Random;

/**
 *
 * @author pedroj
 */
public interface Constantes {
    public static final Random aleatorio = new Random();
    
    // Constantes
    public static final int NUM_PERSONAS = 100;
    public static final int MIN_EQUIPAJE = 2;
    public static final int MAX_EQUIPAJE = 5;
    public static final int NUM_CONSIGNAS = 10;
    public static final int EMPLEADOS = 5;
    public static final int OCUPADO = 0;
    public static final int LIBRE = 1;
    public static final int SIN_CONSIGNA = -1;
    public static final int TIEMPO_OCUPADO = 3; // Tiempo que se ocupa la consigna
    public static final int TIEMPO_ESPERA = 30; // segundos
}
