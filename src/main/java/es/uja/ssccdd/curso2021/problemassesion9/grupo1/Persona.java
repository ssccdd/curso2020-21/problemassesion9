/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion9.grupo1;

import java.util.concurrent.atomic.AtomicIntegerArray;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo1.Constantes.ENTRADAS_AGOTADAS;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo1.Constantes.TIEMPO_VENTA;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Persona implements Runnable {
    private final String iD;
    private final int numEntradas;
    private final int sala;
    private final AtomicIntegerArray aforoSalas;

    public Persona(String iD, int numEntradas, int sala, AtomicIntegerArray aforoSalas) {
        this.iD = iD;
        this.numEntradas = numEntradas;
        this.sala = sala;
        this.aforoSalas = aforoSalas;
    }

    @Override
    public void run() {
        System.out.println("TAREA-" + iD + " Empieza su ejecución");
        
        try {
            
            comprarEntradas();
            
            System.out.println("Ha finalizado la ejecución del TAREA-" + iD);
        } catch (InterruptedException ex) {
            System.out.println("Se ha interrumpido la ejecución del TAREA-" + iD + ex);
        }
    }
    
    private void comprarEntradas() throws InterruptedException {
        int entradasCompradas = 0;
        boolean aforoCompleto = false;
        
        // Comprueba la solicitud de interrupción de la tarea antes de comenzar
        if ( Thread.interrupted() )
           throw new InterruptedException();
        
        // Realizamos la compra entrada a entrada o hasta que se complete el aforo
        while( (entradasCompradas < numEntradas) && !aforoCompleto) {
            
            if( aforoSalas.decrementAndGet(sala) > ENTRADAS_AGOTADAS )
                entradasCompradas++;
            else
                aforoCompleto = true;
            
            // Simulamos el tiempo de venta
            TimeUnit.SECONDS.sleep(TIEMPO_VENTA);
        }
        
        System.out.println("TAREA-" + iD + " Ha comprado " + entradasCompradas + 
                           " entradas y quería comprar " + numEntradas + " entradas");
    }
}
