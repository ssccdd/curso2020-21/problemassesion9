/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion9.grupo3;

import java.util.Random;

/**
 *
 * @author pedroj
 */
public interface Constantes {
    public static final Random aleatorio = new Random();
    
    // Productos
    public enum Producto { 
        LECHE(5), QUESO(10), CARNE(8), PESCADO(5), FRUTA(10), VERDURA(10);
        
        private int unidades;

        private Producto(int unidades) {
            this.unidades = unidades;
        }

        public int getUnidades() {
            return unidades;
        }
    }
    public static final int NUM_PRODUCTOS = Producto.values().length;
    
    // Constantes
    public static final int NUM_CLIENTES = 100;
    public static final int NUM_ESTANTERIAS = 5;
    public static final int AFORO = 15;
    public static final int NO_HAY_PRODUCTO = 0;
    public static final int TIEMPO_REPOSICION = 8; // Tiempo para activar los reponedores
    public static final int TIEMPO_PRODUCTO = 1;
    public static final int TIEMPO_ESPERA = 30; // segundos
}
