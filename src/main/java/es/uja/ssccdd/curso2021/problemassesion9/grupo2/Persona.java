/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion9.grupo2;

import static es.uja.ssccdd.curso2021.problemassesion9.grupo2.Constantes.LIBRE;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo2.Constantes.NUM_CONSIGNAS;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo2.Constantes.OCUPADO;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo2.Constantes.SIN_CONSIGNA;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo2.Constantes.TIEMPO_OCUPADO;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 *
 * @author pedroj
 */
public class Persona implements Runnable {
    private final String iD;
    private final int equipaje;
    private final AtomicIntegerArray consignas;
    private final int[] capacidadConsigna;

    public Persona(String iD, int equipaje, AtomicIntegerArray consignas, int[] capacidadConsigna) {
        this.iD = iD;
        this.equipaje = equipaje;
        this.consignas = consignas;
        this.capacidadConsigna = capacidadConsigna;
    }
    
    @Override
    public void run() {
        System.out.println("TAREA-" + iD + " Empieza su ejecución");
        
        try {
            int consignaOcupada;
            
            consignaOcupada = guardarEquipaje();
            
            if( consignaOcupada == SIN_CONSIGNA )
                System.out.println("Ha finalizado la ejecución del TAREA-" + iD +
                        " por no encontrar una consigna libre");
            else {
                System.out.println("TAREA-" + iD + " Ha ocupado la consigna " + consignaOcupada);
                
                recuperarEquipaje(consignaOcupada);
            
                System.out.println("Ha finalizado la ejecución del TAREA-" + iD);
            }
        } catch (InterruptedException ex) {
            System.out.println("Se ha interrumpido la ejecución del TAREA-" + iD + ex);
        }
        
    }
    
    private int guardarEquipaje() throws InterruptedException {
        int consignaOcupada = SIN_CONSIGNA;
        int consignaActual = 0;
        boolean encontrada = false;
        
        // Comprueba la solicitud de interrupción de la tarea antes de comenzar
        if ( Thread.interrupted() )
           throw new InterruptedException();
        
        // Buscamos una consigna donde dejar el equipaje
        while( (consignaActual < NUM_CONSIGNAS) && !encontrada ) {
            if( (capacidadConsigna[consignaActual] >= equipaje) && 
                consignas.compareAndSet(consignaActual, LIBRE, OCUPADO) ) {
                
                consignaOcupada = consignaActual;
                encontrada = true;
            } else
                consignaActual++;
        } 
        
        return consignaOcupada;
    }
    
    private void recuperarEquipaje(int consignaOcupada) throws InterruptedException {
        // Simulamos el tiempo que la consigna está ocupada
        TimeUnit.SECONDS.sleep(TIEMPO_OCUPADO);
        
        System.out.println("TAREA-" + iD + " recupera el equipaje de la consigna " + consignaOcupada);
        consignas.getAndSet(consignaOcupada, LIBRE);
    }
}
