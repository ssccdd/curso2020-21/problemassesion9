/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion9.grupo6;

import static es.uja.ssccdd.curso2021.problemassesion9.grupo6.Utils.VALOR_GENERACION;
import static es.uja.ssccdd.curso2021.problemassesion9.grupo6.Utils.FabricanteVacuna;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class AlmacenMedico implements Runnable {

    private final int iD;
    private final BlockingDeque<DosisVacuna> dosis;
    private final AtomicInteger contIdentificadores;
    private final AtomicIntegerArray registroTrabajo;

    public AlmacenMedico(int iD, BlockingDeque<DosisVacuna> dosis, AtomicInteger contIdentificadores, AtomicIntegerArray registroTrabajo) {
        this.iD = iD;
        this.dosis = dosis;
        this.contIdentificadores = contIdentificadores;
        this.registroTrabajo = registroTrabajo;
        ThreadLocalRandom.current();
    }

    private int getSiguienteID() {
        return contIdentificadores.getAndIncrement();
    }

    @Override
    public void run() {

        boolean interrumpido = false;
        System.out.println("Almacén " + iD + " ha empezado.");

        while (!interrumpido) {

            try {

                FabricanteVacuna fabricante = FabricanteVacuna.getFabricanteAleatorio(ThreadLocalRandom.current().nextInt(VALOR_GENERACION));
                dosis.put(new DosisVacuna(getSiguienteID(), fabricante));
                registroTrabajo.getAndIncrement(iD);

                TimeUnit.MILLISECONDS.sleep(Utils.TIEMPO_ESPERA_ALMACEN);
            } catch (InterruptedException ex) {
                interrumpido = true;
            }

        }

        System.out.println("El almacén médico " + iD + " ha terminado el trabajo.\tTotal dosis generadas: " + registroTrabajo.get(iD));
    }

}
