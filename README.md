﻿[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 9

Problemas propuestos para la Sesión 9 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2020-21.

En esta sesión de prácticas se proponen ejercicio para trabajar con el paquete [java.util.concurrent](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/package-frame.html). En los ejercicios nos centramos en la utilización de variables atómicas definidas en [java.util.concurrent.atomic](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/package-frame.html). Seguimos utilizando la interface [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html "interface in java.util.concurrent") para la ejecución de las tareas. Los ejercicios son diferentes para cada grupo:

- [Grupo 1](https://gitlab.com/ssccdd/curso2020-21/problemassesion9#grupo-1)
- [Grupo 2](https://gitlab.com/ssccdd/curso2020-21/problemassesion9#grupo-2)
- [Grupo 3](https://gitlab.com/ssccdd/curso2020-21/problemassesion9#grupo-3)
- [Grupo 4](https://gitlab.com/ssccdd/curso2020-21/problemassesion9#grupo-4)
- [Grupo 5](https://gitlab.com/ssccdd/curso2020-21/problemassesion9#grupo-5)
- [Grupo 6](https://gitlab.com/ssccdd/curso2020-21/problemassesion9#grupo-6)

### Grupo 1

Para este ejercicio se tendrá que utilizar variables atómicas y array atómicos para resolver el problema propuesto. Hay que asegurarse que se utilizan los métodos seguros para el intercambio de información entre los diferentes hilos que utilicen estas variables.

Tenemos un sistema de venta de entradas para diferentes salas. Los puntos de venta son **N** y las salas son **M**. Cada sala tiene un aforo diferente. Realizar una implementación que resuelva el problema propuesto. Los clientes deberán implementarse como hilos que realizan una compra para una sala en concreto y por un número de entradas. Los hilos necesarios, así como el hilo principal, deberán ser definidos y completados por el alumno. Además se tienen que definir las constantes necesarias para la prueba del ejercicio.

### Grupo 2

Para este ejercicio se tendrá que utilizar variables atómicas y array atómicos para resolver el problema propuesto. Hay que asegurarse que se utilizan los métodos seguros para el intercambio de información entre los diferentes hilos que utilicen estas variables.

Tenemos un sistema de consigna donde se almacena el equipaje de pasajeros en tránsito. El número de empleados en la consigna es de **N** y el número de consignas es **M**. La capacidad en cada consigna es variable. El pasajero dejará al empleado el número de bultos que forma su equipaje y pasará a recogerlo pasado un tiempo. Realizar una implementación que resuelva el problema propuesto y la definición de las constantes necesarias.

### Grupo 3

Para este ejercicio se tendrá que utilizar variables atómicas y array atómicos para resolver el problema propuesto. Hay que asegurarse que se utilizan los métodos seguros para el intercambio de información entre los diferentes hilos que utilicen estas variables.

Tenemos un número **N** de reponedores de productos en **M** estanterías de una gran superficie comercial. Los reponedores pondrán los productos que falten hasta completar la capacidad de cada estantería cada cierto tiempo. Los productos serán retirados por los clientes en el proceso natural de la venta en la gran superficie. Realizar una implementación que resuelva este problema y la definición de las constantes necesarias.

### Grupo 4

En esta práctica vamos a modificar la práctica de la sesión anterior usando las nuevas herramientas disponibles en el guión de esta semana. Los cambios por clase son:

 - Cliente:
	 - Se modificará el getSiguienteID para que use un [`AtomicInteger`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/AtomicInteger.html) en vez de un contador estático. Para que funcione correctamente es necesario eliminar la palabra clave static del método.
 - Restaurante:
	 - Se modificará el getSiguienteID al igual que en el cliente.
	 - Se llevará un contador de trabajo para comprobar que todos los restaurantes generan los platos correctamente, este registro se mantendrá en un [`AtomicIntegerArray`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/AtomicIntegerArray.html), cada restaurante ira incrementando la posición igual a su ID por cada plato creado e insertado. Para que esto funcione, los IDs de restaurantes deben ser consecutivos e iniciar en 0, o ajustar el acceso si empiezan en 1.
 - Repartidor:
	 - Se llevará un contador de trabajo para comprobar que todos los repartidores recogen los menús correctamente, al igual que en los restaurantes, en este caso se incrementará por cada menú cogido de la cola.
 - Hilo principal:
	 - Creará los contadores (enteros atomic) y arrays atómicos necesarios y se los pasará a los procesos.
	 - Al finalizar imprimirá el contenido de los contadores y arrays.
 - Utils:
	 - Se eliminirá el random en Utils y donde se estaba usando, se cambiará por [`ThreadLocalRandom`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ThreadLocalRandom.html).

En esta sesión no se proporciona un proyecto de netbeans, se puede coger la solución del profesor o el proyecto entregado la semana anterior. En caso de elegir la segunda opción hay que tener en cuenta las anotaciones que indicó el profesor en la entrega. Al inicio de la sesión se explicará como clonar el proyecto del profesor desde gitlab.

### Grupo 5

En esta práctica vamos a modificar la práctica de la sesión anterior usando las nuevas herramientas disponibles en el guión de esta semana. Los cambios por clase son:

 - Cliente:
	 - Se modificará el getSiguienteID para que use un [`AtomicInteger`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/AtomicInteger.html) en vez de un contador estático. Para que funcione correctamente es necesario eliminar la palabra clave static del método.
	 - Se llevará un contador de trabajo para comprobar que todos los clientes  generan las peticiones correctamente, este registro se mantendrá en un [`AtomicIntegerArray`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/AtomicIntegerArray.html), cada cliente ira incrementando la posición igual a su ID por cada petición creada e insertada. Para que esto funcione, los IDs de clientes deben ser consecutivos e iniciar en 0, o ajustar el acceso si empiezan en 1.
 - Ingeniero:
	 -  Se llevará un contador de trabajo para comprobar que todos los ingenieros recogen los pedidos correctamente, al igual que en los clientes, en este caso se incrementará por cada modelo cogido y vuelto a insertar.
 - Máquina de postprocesado:
	 -  Se llevará un contador de trabajo para comprobar que todos las máquinas recogen los modelos correctamente, al igual que en los clientes, en este caso se incrementará por cada modelo cogido y vuelto a insertar.
 - Hilo principal:
	 - Creará los contadores (enteros atomic) y arrays atómicos necesarios y se los pasará a los procesos.
	 - Al finalizar imprimirá el contenido de los contadores y arrays.
 - Utils:
	 - Se eliminirá el random en Utils y donde se estaba usando, se cambiará por [`ThreadLocalRandom`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ThreadLocalRandom.html).

En esta sesión no se proporciona un proyecto de netbeans, se puede coger la solución del profesor o el proyecto entregado la semana anterior. En caso de elegir la segunda opción hay que tener en cuenta las anotaciones que indicó el profesor en la entrega. Al inicio de la sesión se explicará como clonar el proyecto del profesor desde gitlab.

### Grupo 6

En esta práctica vamos a modificar la práctica de la sesión anterior usando las nuevas herramientas disponibles en el guión de esta semana. Los cambios por clase son:

 - Centro:
	 - Se modificará el getSiguienteID para que use un [`AtomicInteger`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/AtomicInteger.html) en vez de un contador estático. Para que funcione correctamente es necesario eliminar la palabra clave static del método.
	 - Se llevará un contador de trabajo para comprobar que todos los centros generan pacientes correctamente, este registro se mantendrá en un [`AtomicIntegerArray`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/AtomicIntegerArray.html), cada centro ira incrementando la posición igual a su ID por cada paciente creado e insertado. Para que esto funcione, los IDs de centros deben ser consecutivos e iniciar en 0, o ajustar el acceso si empiezan en 1.
 - Almacén:
	 - Se modificará el getSiguienteID al igual que en el centro.
	 - Se llevará un contador de trabajo para comprobar que todos los almacenes insertan las dosis correctamente, en este caso se incrementará por cada dosis insertada.
 - Enfermero:
	 -  Se llevará un contador de trabajo para comprobar que todos los enfermeros recogen los pacientes y dosis correctamente, en este caso se incrementará por cada paciente cogido de la cola.
 - Hilo principal:
	 - Creará los contadores (enteros atomic) y arrays atómicos necesarios y se los pasará a los procesos.
	 - Al finalizar imprimirá el contenido de los contadores y arrays.
 - Utils:
	 - Se eliminirá el random en Utils y donde se estaba usando, se cambiará por [`ThreadLocalRandom`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ThreadLocalRandom.html).

En esta sesión no se proporciona un proyecto de netbeans, se puede coger la solución del profesor o el proyecto entregado la semana anterior. En caso de elegir la segunda opción hay que tener en cuenta las anotaciones que indicó el profesor en la entrega. Al inicio de la sesión se explicará como clonar el proyecto del profesor desde gitlab.